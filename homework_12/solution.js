module.exports = Collection;

/**
 * Конструктор коллекции
 */
function Collection() {}

// Методы коллекции
Collection.prototype.values = function () {
    var values = [];

    for (var key in this) {
        if (this.hasOwnProperty(key)) {
            values.push(this[key]);
        }
    }
    return values;
};

Collection.prototype.at = function (pos) {
    var res = null,
        counter = 0;

    for (var key in this) {
        counter++;
        if (counter === pos && this.hasOwnProperty(key)) {
            res = this[key];
        }
    }
    return res;
};

Collection.prototype.append = function (elem) {
    if (elem instanceof Object) {
        for (var key in elem) {
            if (elem.hasOwnProperty(key)) {
                this[key] = elem[key];
            }
        }
    } else {
        this[elem] = elem;
    }
};

Collection.prototype.removeAt = function (pos) {
    var res = false,
        counter = 0;

    for (var key in this) {
        counter++;
        if (counter === pos && this.hasOwnProperty(key)) {
            delete this[key];
            res = true;
        }
    }
    return res;
};

Collection.prototype.count = function () {
    var counter = 0;

    for (var key in this) {
        if (this.hasOwnProperty(key)) {
            counter++;
        }
    }
    return counter;
};

/**
 * Создание коллекции из массива значений
 */
Collection.from = function (args) {
    var arr = args.constructor == Array ? args : [].slice.call(arguments);

    return arr.reduce(function (acc, cur) {
        acc[cur] = cur;
        return acc;
    }, new Collection);
};