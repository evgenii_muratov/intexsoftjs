var phoneBook = [];



function add(cmd) {
    var i;

    phoneBook.forEach(function (item, pos) {
        i = item.endsWith(cmd[1], cmd[1].length) ? pos : -1;
    });

    if (i === -1 || i === undefined) {
        phoneBook = phoneBook.concat(cmd[1] + ': ' + cmd.slice(2).join(', '));
    } else if (/[0-9]/.test(phoneBook[i])) {
        phoneBook[i] += ', ' + cmd.slice(2).join(', ');
    } else {
        phoneBook[i] += cmd.slice(2).join(', ');
    }



};

function remove(cmd) {
    var success = false;

    for (var i = 0; i < phoneBook.length; i++) {
        var a = phoneBook[i].indexOf(cmd[1]);

        if (a !== -1) {
            phoneBook[i] = phoneBook[i].substr(0, a) + phoneBook[i].substr(a + cmd[1].length + 2);

            if (phoneBook[i].endsWith(',')) {
                phoneBook[i] = phoneBook[i].slice(0, phoneBook[i].length - 1);
            }
            success = true;
        }
    }
    return success;
};

function show(cmd) {
    var sortPhoneBook = [];

    for (i = 0; i < phoneBook.length; i++) {
        if (/[0-9]/.test(phoneBook[i])) {
            sortPhoneBook.push(phoneBook[i]);
        }
    }
    sortPhoneBook.sort();
    
    return sortPhoneBook;
};


module.exports = {
    getWords: function (sentence) {
        var tags = sentence.split(' ')
            .filter(function (item) {
                return item.substring(0, 1) === '#';
            })
            .map(function (item) {
                return item.substring(1);
            })
            .filter(function (item) {
                return item !== '';
            });
        return tags;
    },

    normalizeWords: function (words) {
        var result = words.map(function (item) {
            return item.toLowerCase()
        })
            .filter(function (item, pos, arr) {
                return arr.indexOf(item) == pos;
            })
            .join(', ');
        return result;
    },

    addressBook: function (command) {
        var cmd = command.split(/[\s,]+/), func;

        switch (cmd[0]) {
            case 'ADD':
                func = add(cmd);
                break;
            case 'REMOVE_PHONE':
                func = remove(cmd);
                break;
            case 'SHOW':
                func = show(cmd);
                break;
            default:
                alert('Incorrect input');
        }
        return func;
    }
}