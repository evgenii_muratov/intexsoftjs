function query(collection) {
    var filterRes = arguments[0];

    for (var i = 1; i < arguments.length; i++) {
        if (arguments[i].name == 'filterIn') {
            filterRes = arguments[i].action(filterRes);
        }
    }
    var result = filterRes;

    for (var i = 1; i < arguments.length; i++) {
        if (arguments[i].name == 'select') {
            result = arguments[i].action(result);
        }
    }

    if (!arguments[1]) {
        if (arguments[0]) {
            result = arguments[0].slice();
        } else {
            alert('no data')
        }
    }
    return result;
}

function select() {
    var args = [].slice.call(arguments);
    return {
        name: 'select',
        action: function (data) {
            var fields = args;

            return data.map(function (value) {
                var item = {};
                fields.forEach(function (field) {
                    if (value[field] !== undefined) {
                        item[field] = value[field];
                    }
                })

                return item;
            });
        }
    }
}

function filterIn(property, values) {
    var args = arguments;
    return {
        name: 'filterIn',
        action: function (data) {
            var property = args[0],
                values = args[1];
            return data.filter(function (value) {
                return values.indexOf(value[property]) > -1;
            });
        }
    }
}

module.exports = {
    timeShift: function (date) {

        return {

            valueDate: new Date(date),

            methods: {
                years: 'FullYear',
                months: 'Month',
                days: 'Date',
                hours: 'Hours',
                minutes: 'Minutes'
            },

            add: function (num, command) {
                this.format(new Date(this.valueDate['set' + this.methods[command]]
                    (this.valueDate['get' + this.methods[command]]() + num)));
                return this;
            },

            subtract: function (num, command) {
                this.format(new Date(this.valueDate['set' + this.methods[command]]
                    (this.valueDate['get' + this.methods[command]]() - num)));
                return this;
            },

            format: function (arg) {
                var year = arg.getFullYear(),
                    month = arg.toLocaleString("ru", { month: '2-digit' }),
                    day = arg.toLocaleString("ru", { day: '2-digit' }),
                    time = arg.toLocaleString("ru", { hour: '2-digit', minute: '2-digit' });
                this.value = year + "-" + month + "-" + day + " " + time;
            }
        }
    },
    lib: {
        query: query,
        select: select,
        filterIn: filterIn
    }
};
